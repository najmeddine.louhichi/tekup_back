package de.tekup.modeles;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Delivery {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "etat")
    @NonNull
    private String etat;

    @Column(name = "dateLivraison")
    @DateTimeFormat
    private Date dateLivraison;

    @Column(name = "mode")
    @NonNull
    private String mode;

    @OneToOne(mappedBy = "livraison")
    private Command cmde;

}

package de.tekup.modeles;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import javax.persistence.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Product(Long id, String designation, Long prix, int qte, Boolean dispo) {
        super();
        this.id = id;
        this.designation = designation;
        this.prix = prix;
        this.qte = qte;
        this.dispo = dispo;
    }

    @Column(name = "designation")
    @NonNull
    private String designation;

    @Column(name = "prix")
    @NonNull
    private Long prix;

    @Column(name = "quantite")
    private int qte;

    @Column(name = "disponibilite")
    @NonNull
    private Boolean dispo;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @ManyToOne
    @JoinColumn(name = "sub_category_id")
    private SubCategory subCategory;

    @ManyToOne
    @JoinColumn(name = "brand_id")
    private Brand brand;

    @OneToMany(mappedBy = "prod")
    private List<Supplier> Suppliers;

    @OneToMany(mappedBy = "produit")
    private List<CommandLine> lignes;

}

package de.tekup.modeles;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Supplier {

    @Id
    private Long id;

    @Column(name = "nom_user", length = 50)
    @NonNull
    private String nom;

    @Column(name = "user_prenom", length = 50)
    @NonNull
    private String prenom;

    @Column
    @Email
    private String email;

    @Column(name = "num_tel")
    @Size(max = 8, min = 8)
    @NonNull
    private String tel;

    @Column(name = "adresse")
    @NonNull
    private String adresse;

    @Column(name = "sexe")
    @NonNull
    private String sexe;

    @Column
    private String login;

    @Column(name = "password")
    @Size(min = 6)
    @NonNull
    private String password;

    @Enumerated(EnumType.STRING)
    private Role role;

    public void setRole(Role role) {
        // TODO Auto-generated method stub
        setRole(Role.Supplier);
    }

    @ManyToOne
    @JoinColumn(name = "produit_id")
    private Product prod;

}

package de.tekup.modeles;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Command {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "total")
    @NonNull
    private Long total;

    @Column(name = "dateCommande")
    @DateTimeFormat
    private Date date;

    @Column(name = "etat")
    @NonNull
    private String etat;

    @OneToOne
    @JoinColumn(name = "livraison_id")
    private Delivery livraison;

    @OneToOne(mappedBy = "commande")
    private Facture facture;

    @OneToMany(mappedBy = "cmde")
    private List<CommandLine> ligness;

}

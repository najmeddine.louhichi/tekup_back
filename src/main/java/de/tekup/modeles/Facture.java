package de.tekup.modeles;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Facture {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date")
    @DateTimeFormat
    private Date date;

    @Column(name = "PrixHt")
    private Long prixHT;

    @Column(name = "TVA")
    private Long tva;

    @Column(name = "remise")
    private Long remise;

    @Column(name = "totalHorsTaxe")
    private Long totalHT;

    @Column(name = "totalTT")
    private Long totalTT;

    @OneToOne
    @JoinColumn(name = "commande_id")
    private Command commande;

}

package de.tekup.modeles;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Entity
@AllArgsConstructor
@Data
@NoArgsConstructor
public class User {

    @Id
    private Long id;

    @Column(name = "nom_user", length = 50)
    @NonNull
    private String nom;

    @Column(name = "user_prenom", length = 50)
    @NonNull
    private String prenom;

    @Column
    @Email
    private String email;

    @Column(name = "num_tel")
    @Size(max = 8, min = 8)
    @NonNull
    private String tel;

    @Column(name = "adresse")
    @NonNull
    private String adresse;

    @Column(name = "sexe")
    @NonNull
    private String sexe;

    @Column
    private String login;

    @Column(name = "password")
    @Size(min = 6)
    @NonNull
    private String password;

    @Enumerated(EnumType.STRING)
    private Role role;

    @OneToMany(mappedBy = "utilisateur")
    private List<Reclamation> reclamations;

}

package de.tekup.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import de.tekup.modeles.Command;
import de.tekup.repositories.CommandRepo;

@RestController
@RequestMapping("/command")
public class CommandController {

    private final CommandRepo repo;

    @Autowired
    public CommandController(CommandRepo repo) {
        this.repo = repo;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/add", produces = MediaType.APPLICATION_JSON_VALUE)
    public void saveOrUpdateScrapedCommand(@Valid @RequestBody Command cmd) {
        repo.save(cmd);
    }

    @GetMapping(value = "/")
    public List<Command> getAllCommand() {
        return repo.findAll();
    }

    @GetMapping(value = "/id/{id}")
    public Command getCommandID(@PathVariable("id") Long id) {
        return repo.findCommandById(id);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Command> updateCommand(@PathVariable("id") Long id, @RequestBody Command cmd) {

        Command c = repo.findCommandById(id);
        if (cmd.getId() != null) {
            c.setId(cmd.getId());
        }
        if (cmd.getLigness() != null) {
            c.setLigness(cmd.getLigness());
        }
        if (cmd.getDate() != null) {
            c.setDate(cmd.getDate());
        }
        if (cmd.getEtat() != null) {
            c.setEtat(cmd.getEtat());
        }
        if (cmd.getTotal() != 0) {
            c.setTotal(cmd.getTotal());
        }
        if (cmd.getFacture() != null) {
            c.setFacture(cmd.getFacture());
        }
        if (cmd.getLivraison() != null) {
            c.setLivraison(cmd.getLivraison());
        }

        repo.save(c);
        return new ResponseEntity<>(cmd, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteScrapedCommand(@PathVariable("id") Long id) {
        repo.delete(repo.findCommandById(id));
    }

}

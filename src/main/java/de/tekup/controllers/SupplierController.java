package de.tekup.controllers;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import de.tekup.modeles.Supplier;
import de.tekup.services.SupplierService;

@RestController
@RequestMapping("/supplier")
public class SupplierController {

    @Autowired
    private SupplierService supserv;

    @GetMapping("/list")
    public List<Supplier> getAllSuppliers() {
        return supserv.getAllSupplier();
    }

    @PostMapping("/add")
    public Supplier addNewSupplier(@RequestBody Supplier supplier) {
        return supserv.addSupplier(supplier);
    }

    @PutMapping("/edit/{id}")
    public ResponseEntity<Supplier> updateSupplier(@PathVariable("id") Long id, @Valid @RequestBody Supplier supplier) {

        Supplier s = supserv.findById(id).get();

        if (s != null && supplier != null) {
            if (supplier.getAdresse() != null) {
                s.setAdresse(supplier.getAdresse());
            }
            if (supplier.getEmail() != null) {
                s.setEmail(supplier.getEmail());
            }
            if (supplier.getId() > 0) {
                s.setId(supplier.getId());
            }
            if (supplier.getLogin() != null) {
                s.setLogin(supplier.getLogin());
            }
            if (supplier.getNom() != null) {
                s.setNom(supplier.getNom());
            }
            if (supplier.getPassword() != null) {
                s.setPassword(supplier.getPassword());
            }
            if (supplier.getPrenom() != null) {
                s.setPrenom(supplier.getPrenom());
            }
            if (supplier.getRole() != null) {
                s.setRole(supplier.getRole());
            }
            if (supplier.getSexe() != null) {
                s.setSexe(supplier.getSexe());
            }
            if (supplier.getTel() != null) {
                s.setTel(supplier.getTel());
            }

            supserv.addSupplier(s);

        }
        supserv.addSupplier(s);
        return new ResponseEntity<Supplier>(s, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public List<Supplier> deleteScrapedProduct(@PathVariable("id") Long id) {
        Optional<Supplier> supp = supserv.findById(id);
        Supplier s = supp.get();
        supserv.delete(s.getId());
        return supserv.getAllSupplier();
    }

}

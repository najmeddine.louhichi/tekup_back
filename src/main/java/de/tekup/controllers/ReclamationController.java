package de.tekup.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import de.tekup.modeles.Reclamation;
import de.tekup.repositories.ReclamationRepo;

@RestController
@RequestMapping("/reclamation")
public class ReclamationController {

    private final ReclamationRepo repo;

    @Autowired
    public ReclamationController(ReclamationRepo repo) {

        this.repo = repo;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/add", produces = MediaType.APPLICATION_JSON_VALUE)
    public void saveOrUpdateScrapedReclamation(@Valid @RequestBody Reclamation rec) {
        repo.save(rec);
    }

    @GetMapping(value = "/")
    public List<Reclamation> getAllReclamation() {
        return repo.findAll();
    }

    @GetMapping(value = "/id/{id}")
    public Reclamation getReclamationID(@PathVariable("id") Long id) {
        return repo.findReclamationById(id);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Reclamation> updateReclamation(@PathVariable("id") Long id, @RequestBody Reclamation rec) {

        Reclamation b = repo.findReclamationById(id);
        if (rec.getId() != null) {
            b.setId(rec.getId());
        }
        if (rec.getText() != null) {
            b.setText(rec.getText());
        }
        if (rec.getUtilisateur() != null) {
            b.setUtilisateur(rec.getUtilisateur());
        }
        if (rec.getUtilisateur() != null) {
            b.setUtilisateur(rec.getUtilisateur());
        }
        if (rec.getProduct() != null) {
            b.setProduct(rec.getProduct());
        }

        repo.save(b);
        return new ResponseEntity<Reclamation>(rec, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteScrapedReclamation(@PathVariable("id") Long id) {
        repo.delete(repo.findReclamationById(id));
    }

}

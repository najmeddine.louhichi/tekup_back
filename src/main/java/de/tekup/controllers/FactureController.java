package de.tekup.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import de.tekup.modeles.Facture;
import de.tekup.repositories.FactureRepo;

@RestController
@RequestMapping("/facture")
public class FactureController {

    private final FactureRepo repo;

    @Autowired
    public FactureController(FactureRepo repo) {
        this.repo = repo;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/add", produces = MediaType.APPLICATION_JSON_VALUE)
    public void saveOrUpdateScrapedFacture(@Valid @RequestBody Facture fac) {
        repo.save(fac);
    }

    @GetMapping(value = "/")
    public List<Facture> getAllFacture() {
        return repo.findAll();
    }

    @GetMapping(value = "/id/{id}")
    public Facture getFactureID(@PathVariable("id") Long id) {
        return repo.findFactureById(id);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Facture> updateFacture(@PathVariable("id") Long id, @RequestBody Facture fac) {

        Facture c = repo.findFactureById(id);
        if (fac.getId() != null) {
            c.setId(fac.getId());
        }
        if (fac.getCommande() != null) {
            c.setCommande(fac.getCommande());
        }
        if (fac.getDate() != null) {
            c.setDate(fac.getDate());
        }
        if (fac.getPrixHT() != 0) {
            c.setPrixHT(fac.getPrixHT());
        }

        if (fac.getRemise() != null) {
            c.setRemise(fac.getRemise());
        }
        if (fac.getTotalHT() != 0) {
            c.setTotalHT(fac.getTotalHT());
        }
        if (fac.getTotalTT() != 0) {
            c.setTotalTT(fac.getTotalTT());
        }
        if (fac.getTva() != 0) {
            c.setTva(fac.getTva());
        }
        repo.save(c);
        return new ResponseEntity<Facture>(fac, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteScrapedFacture(@PathVariable("id") Long id) {
        repo.delete(repo.findFactureById(id));
    }

}

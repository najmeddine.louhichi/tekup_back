package de.tekup.controllers;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import de.tekup.modeles.User;
import de.tekup.services.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService supserv;

    @GetMapping("/list")
    public List<User> getAllUsers() {
        return supserv.getAllUser();
    }

    @PostMapping("/add")
    public User addNewUser(@RequestBody User user) {
        return supserv.addUser(user);
    }

    @PutMapping("/edit/{id}")
    public ResponseEntity<User> updateUser(@PathVariable("id") Long id, @Valid @RequestBody User user) {

        User s = supserv.findById(id).get();

        if (s != null) {
            if (user.getAdresse() != null) {
                s.setAdresse(user.getAdresse());
            }
            if (user.getEmail() != null) {
                s.setEmail(user.getEmail());
            }
            if (user.getId() > 0) {
                s.setId(user.getId());
            }
            if (user.getLogin() != null) {
                s.setLogin(user.getLogin());
            }
            if (user.getNom() != null) {
                s.setNom(user.getNom());
            }
            if (user.getPassword() != null) {
                s.setPassword(user.getPassword());
            }
            if (user.getPrenom() != null) {
                s.setPrenom(user.getPrenom());
            }
            if (user.getRole() != null) {
                s.setRole(user.getRole());
            }
            if (user.getSexe() != null) {
                s.setSexe(user.getSexe());
            }
            if (user.getReclamations() != null) {
                s.setReclamations(user.getReclamations());
            }
            if (user.getTel() != null) {
                s.setTel(user.getTel());
            }

        }
        supserv.addUser(s);
        return new ResponseEntity<User>(s, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public List<User> deleteScrapedUser(@PathVariable("id") Long id) {
        Optional<User> user = supserv.findById(id);
        User u = user.get();
        supserv.delete(u.getId());
        return supserv.getAllUser();
    }

}

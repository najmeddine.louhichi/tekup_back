package de.tekup.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import de.tekup.modeles.Category;
import de.tekup.modeles.SubCategory;
import de.tekup.repositories.CategoryRepo;
import de.tekup.repositories.SubCategoryRepo;
import java.util.ArrayList;
import java.util.Optional;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@RequestMapping("/category")
@CrossOrigin(allowedHeaders = "*", origins = "*")
public class CategoryController {

    private CategoryRepo Repo;
    private SubCategoryRepo subCategoryRepo;

    @Autowired
    public CategoryController(CategoryRepo repo, SubCategoryRepo subCategoryRepo) {

        this.Repo = repo;
        this.subCategoryRepo = subCategoryRepo;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/add", produces = MediaType.APPLICATION_JSON_VALUE)
    public Category saveOrUpdateScrapedProduct(@Valid @RequestBody Category cat) {
        Category savedCategory = Repo.save(cat);
        return savedCategory;

    }

    @GetMapping(value = "/")
    public List<Category> getAllCategories() {
        return Repo.findAll();
    }

    @GetMapping(value = "/id/{categorieID}")
    public Category getCategorieID(@PathVariable("categorieID") Long categorieID) {
        return Repo.findCategorieById(categorieID);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Category> updateCategory(@PathVariable("id") Long id, @RequestBody Category cat) {

        Category categ = Repo.findCategorieById(id);
        if (cat.getDesig() != null) {
            categ.setDesig(cat.getDesig());
        }
        if(cat.getSubCategories()!= null){
            categ.setSubCategories(cat.getSubCategories());
        }

        Category savedCategory = Repo.save(categ);
        return new ResponseEntity<>(savedCategory, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteScrapedCategory(@PathVariable("id") Long id) {
        Optional<Category> category = Repo.findById(id);
        
        if (category.isPresent()) {
            subCategoryRepo.deleteAll(category.get().getSubCategories());
            Repo.deleteById(id);
        }
    }

}

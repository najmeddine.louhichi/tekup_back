package de.tekup.controllers;

import java.util.List;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import de.tekup.modeles.Delivery;
import de.tekup.repositories.DeliveryRepo;

@RestController
@RequestMapping("/delivery")
public class DeliveryController {

    private final DeliveryRepo repo;

    @Autowired
    public DeliveryController(DeliveryRepo repo) {
        this.repo = repo;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/add", produces = MediaType.APPLICATION_JSON_VALUE)
    public void saveOrUpdateScrapedDelivery(@Valid @RequestBody Delivery del) {
        repo.save(del);
    }

    @GetMapping(value = "/")
    public List<Delivery> getAllDelivery() {
        return repo.findAll();
    }

    @GetMapping(value = "/id/{id}")
    public Delivery getDeliveryID(@PathVariable("id") Long id) {
        return repo.findDeliveryById(id);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Delivery> updateDelivery(@PathVariable("id") Long id, @RequestBody Delivery del) {

        Delivery c = repo.findDeliveryById(id);
        if (del.getId() != null) {
            c.setId(del.getId());
        }
        if (del.getEtat() != null) {
            c.setEtat(del.getEtat());
        }
        if (del.getCmde() != null) {
            c.setCmde(del.getCmde());
        }
        if (del.getMode() != null) {
            c.setMode(del.getMode());
        }

        if (del.getDateLivraison() != null) {
            c.setDateLivraison(del.getDateLivraison());
        }

        repo.save(c);
        return new ResponseEntity<Delivery>(del, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteScrapedDelivery(@PathVariable("id") Long id) {
        repo.delete(repo.findDeliveryById(id));
    }

}

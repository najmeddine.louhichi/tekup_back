package de.tekup.controllers;

import de.tekup.modeles.SubCategory;
import de.tekup.repositories.SubCategoryRepo;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SubCategoryController {

    private SubCategoryRepo repo;

    @Autowired
    public SubCategoryController(SubCategoryRepo repo) {

        this.repo = repo;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/add", produces = MediaType.APPLICATION_JSON_VALUE)
    public SubCategory saveOrUpdateSubCategory(@Valid @RequestBody SubCategory subCategory) {
        return repo.save(subCategory);

    }

    @GetMapping(value = "/")
    public List<SubCategory> getAllSubCategories() {
        return repo.findAll();
    }

    @GetMapping(value = "/id/{subCategoryID}")
    public Optional<SubCategory> findSubCategoryById(@PathVariable("subCategoryID") Long subCategoryId) {
        return repo.findById(subCategoryId);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<SubCategory> updateSubCategory(@PathVariable("id") Long id, @RequestBody SubCategory subCategory) {

        Optional<SubCategory> duplicatedSubCategory = repo.findById(id);
        if (duplicatedSubCategory.isPresent() && subCategory.getName() != null) {
            subCategory.setId(duplicatedSubCategory.get().getId());
        }
        return new ResponseEntity<>(repo.save(subCategory), HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteScrapedSubCategory(@PathVariable("id") Long id) {
        repo.deleteById(id);
    }

}

package de.tekup.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import de.tekup.modeles.CommandLine;
import de.tekup.repositories.CommandLineRepo;

@RestController
@RequestMapping("/cmdline")
public class CommandLineController {

    private final CommandLineRepo repo;

    @Autowired
    public CommandLineController(CommandLineRepo repo) {
        this.repo = repo;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/add", produces = MediaType.APPLICATION_JSON_VALUE)
    public void saveOrUpdateScrapedCommandLine(@Valid @RequestBody CommandLine cmdLine) {
        repo.save(cmdLine);
    }

    @GetMapping(value = "/")
    public List<CommandLine> getAllCommandLine() {
        return repo.findAll();
    }

    @GetMapping(value = "/id/{id}")
    public CommandLine getCommandLineID(@PathVariable("id") Long id) {
        return repo.findCommandLineById(id);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<CommandLine> updateCommandLine(@PathVariable("id") Long id, @RequestBody CommandLine cmd) {

        CommandLine c = repo.findCommandLineById(id);
        if (cmd.getId() != null) {
            c.setId(cmd.getId());
        }

        if (cmd.getDate() != null) {
            c.setDate(cmd.getDate());
        }

        if (cmd.getQte() != 0) {
            c.setQte(cmd.getQte());
        }
        if (cmd.getCmde() != null) {
            c.setCmde(cmd.getCmde());
        }
        if (cmd.getProduit() != null) {
            c.setProduit(cmd.getProduit());
        }

        repo.save(c);
        return new ResponseEntity<>(cmd, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteScrapedCommandLine(@PathVariable("id") Long id) {
        repo.delete(repo.findCommandLineById(id));
    }

}

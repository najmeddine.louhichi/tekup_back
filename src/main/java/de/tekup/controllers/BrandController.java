package de.tekup.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import de.tekup.modeles.Brand;
import de.tekup.repositories.MarqueRepository;

@RestController
@RequestMapping("/brand")
public class BrandController {

    private final MarqueRepository repo;

    @Autowired
    public BrandController(MarqueRepository repo) {

        this.repo = repo;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/add", produces = MediaType.APPLICATION_JSON_VALUE)
    public void saveOrUpdateScrapedMarque(@Valid @RequestBody Brand brand) {
        repo.save(brand);
    }

    @GetMapping(value = "/")
    public List<Brand> getAllMarque() {
        return repo.findAll();
    }

    @GetMapping(value = "/id/{id}")
    public Brand getMarqueID(@PathVariable("id") Long id) {
        return repo.findMarqueById(id);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Brand> updateMarque(@PathVariable("id") Long id, @RequestBody Brand brand) {

        Brand b = repo.findMarqueById(id);
        if (brand != null && brand.getDesing() != null) {
            b.setDesing(brand.getDesing());
        }

        repo.save(b);
        return new ResponseEntity<>(brand, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{MarqueId}")
    public void deleteScrapedMarque(@PathVariable("MarqueId") Long MarqueId) {
        repo.delete(repo.findMarqueById(MarqueId));
    }

}

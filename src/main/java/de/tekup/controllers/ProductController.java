package de.tekup.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import de.tekup.modeles.Product;
import de.tekup.repositories.ProductRepository;
import de.tekup.services.ProductService;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@RequestMapping("/product")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ProductController {

    private final ProductRepository productRepository;
    private final ProductService productService;

    @Autowired
    public ProductController(ProductRepository productRepository, ProductService productService) {

        this.productRepository = productRepository;
        this.productService = productService;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/add", produces = MediaType.APPLICATION_JSON_VALUE)
    public void saveOrUpdateScrapedProduct(@Valid @RequestBody Product product) {
        productRepository.save(product);

    }

    @GetMapping(value = "/")
    public List<Product> getAllProduct() {
        return productRepository.findAll();
    }

    @GetMapping(value = "/id/{ProductId}")
    public Product getProductID(@PathVariable("ProductId") Long ProductId) {
        return productRepository.findProductById(ProductId);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Product> updateProduct(@PathVariable("id") Long id, @RequestBody Product product) {

        Product p = productRepository.findProductById(id);
        if (product.getDispo() != null) {
            p.setDispo(product.getDispo());
        }
        if (product.getQte() != 0) {
            p.setQte(product.getQte());
        }
        if (product.getPrix() != null) {
            p.setPrix(product.getPrix());
        }
        if (product.getDesignation() != null) {
            p.setDesignation(product.getDesignation());

        }
        if (product.getCategory()!= null) {
            p.setCategory(product.getCategory());

        }
        if (product.getSubCategory()!= null) {
            p.setSubCategory(product.getSubCategory());

        }
        productRepository.save(p);
        return new ResponseEntity<Product>(p, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{ProductId}")
    public void deleteScrapedProduct(@PathVariable("ProductId") Long ProductId) {
        productRepository.delete(productRepository.findProductById(ProductId));
    }

    @GetMapping(value = "/search/category/{categoryId}")
    public List<Product> findProductByCategoryId(@PathVariable("categoryId") String categoryId) {
        return productService.findByCategoryId(Long.parseLong(categoryId));
    }

    @GetMapping(value = "/search/sub-category/{subCategoryId}")
    public List<Product> findProductBySubCategoryId(@PathVariable("subCategoryId") String subCategoryId) {
        return productService.findBySubCategoryId(Long.parseLong(subCategoryId));
    }

}

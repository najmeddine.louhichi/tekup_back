package de.tekup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetSiteweb1Application {

    public static void main(String[] args) {
        SpringApplication.run(ProjetSiteweb1Application.class, args);
    }

}

package de.tekup.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import de.tekup.modeles.Command;

public interface CommandRepo extends JpaRepository<Command, Long> {

    Command findCommandById(Long id);

}

package de.tekup.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import de.tekup.modeles.Category;

@Repository
public interface CategoryRepo extends JpaRepository<Category, Long> {

    Category findCategorieById(Long categorieID);

}

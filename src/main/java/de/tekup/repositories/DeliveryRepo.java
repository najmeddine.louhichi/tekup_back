package de.tekup.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import de.tekup.modeles.Delivery;

public interface DeliveryRepo extends JpaRepository<Delivery, Long> {

    Delivery findDeliveryById(Long id);

}

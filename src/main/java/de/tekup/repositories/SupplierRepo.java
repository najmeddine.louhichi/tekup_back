package de.tekup.repositories;

import org.springframework.data.repository.CrudRepository;

import de.tekup.modeles.Supplier;

public interface SupplierRepo extends CrudRepository<Supplier, Long> {

}

package de.tekup.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import de.tekup.modeles.Reclamation;

public interface ReclamationRepo extends JpaRepository<Reclamation, Long> {

    Reclamation findReclamationById(Long id);

}

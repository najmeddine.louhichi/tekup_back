package de.tekup.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import de.tekup.modeles.Product;
import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    Product findProductById(Long productID);

    public List<Product> findBySubCategoryId(Long subCategoryId);

    public List<Product> findByCategoryId(Long categoryId);
}

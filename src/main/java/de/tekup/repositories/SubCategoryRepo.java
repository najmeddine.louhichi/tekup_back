package de.tekup.repositories;

import de.tekup.modeles.SubCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubCategoryRepo extends JpaRepository<SubCategory, Long> {

}

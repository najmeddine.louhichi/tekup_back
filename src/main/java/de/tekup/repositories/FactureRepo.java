package de.tekup.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import de.tekup.modeles.Facture;

public interface FactureRepo extends JpaRepository<Facture, Long> {

    Facture findFactureById(Long id);

}

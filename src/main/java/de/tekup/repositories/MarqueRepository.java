package de.tekup.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import de.tekup.modeles.Brand;

@Repository
public interface MarqueRepository extends JpaRepository<Brand, Long> {

    Brand findMarqueById(Long MarqurId);

}

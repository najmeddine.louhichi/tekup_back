package de.tekup.repositories;

import org.springframework.data.repository.CrudRepository;

import de.tekup.modeles.User;

public interface UserRepo extends CrudRepository<User, Long> {

}

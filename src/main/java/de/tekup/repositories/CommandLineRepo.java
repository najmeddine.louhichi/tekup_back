package de.tekup.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import de.tekup.modeles.CommandLine;

public interface CommandLineRepo extends JpaRepository<CommandLine, Long> {

    CommandLine findCommandLineById(Long id);

}

package de.tekup.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import de.tekup.modeles.Product;
import de.tekup.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository prodRepo;

    @Override
    public Product addProduct(Product p) {
        // TODO Auto-generated method stub
        return prodRepo.save(p);
    }

    @Override
    public List<Product> getAllProduct() {
        // TODO Auto-generated method stub
        return (List<Product>) prodRepo.findAll();
    }

    @Override
    public void delete(Long id) {
        // TODO Auto-generated method stub
        prodRepo.deleteById(id);

    }

    @Override
    public Optional<Product> findById(Long id) {
        // TODO Auto-generated method stub
        return prodRepo.findById(id);
    }

    @Override
    public List<Product> findByCategoryId(Long categoryId) {
        return prodRepo.findByCategoryId(categoryId);
    }

    @Override
    public List<Product> findBySubCategoryId(Long subCategoryId) {
        return prodRepo.findBySubCategoryId(subCategoryId);
    }

    @Override
    public Product saveProduct(Product product) {
        return prodRepo.save(product);
    }

}

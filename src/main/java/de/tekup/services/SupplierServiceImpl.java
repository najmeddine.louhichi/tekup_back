package de.tekup.services;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import de.tekup.modeles.Supplier;
import de.tekup.repositories.SupplierRepo;

@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierRepo supprepo;

    @Override
    public Supplier addSupplier(Supplier supplier) {
        // TODO Auto-generated method stub
        return supprepo.save(supplier);
    }

    @Override
    public List<Supplier> getAllSupplier() {
        // TODO Auto-generated method stub
        return (List<Supplier>) supprepo.findAll();
    }

    @Override
    public void delete(Long id) {
        supprepo.deleteById(id);
    }

    @Override
    public Optional<Supplier> findById(Long id) {
        // TODO Auto-generated method stub
        return supprepo.findById(id);
    }

}

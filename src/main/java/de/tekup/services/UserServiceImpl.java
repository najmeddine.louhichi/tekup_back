package de.tekup.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.tekup.modeles.User;
import de.tekup.repositories.UserRepo;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepo userrepo;

    @Override
    public User addUser(User user) {
        // TODO Auto-generated method stub
        return userrepo.save(user);
    }

    @Override
    public List<User> getAllUser() {
        // TODO Auto-generated method stub
        return (List<User>) userrepo.findAll();
    }

    @Override
    public void delete(Long id) {
        // TODO Auto-generated method stub
        userrepo.deleteById(id);
    }

    @Override
    public Optional<User> findById(Long id) {
        // TODO Auto-generated method stub
        return userrepo.findById(id);
    }

}

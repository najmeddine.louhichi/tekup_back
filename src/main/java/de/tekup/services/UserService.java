package de.tekup.services;

import java.util.List;
import java.util.Optional;

import de.tekup.modeles.User;

public interface UserService {

    abstract User addUser(User user);

    abstract List<User> getAllUser();

    abstract void delete(Long id);

    abstract Optional<User> findById(Long id);

}

package de.tekup.services;

import java.util.List;
import java.util.Optional;

import de.tekup.modeles.Supplier;

public interface SupplierService {

    abstract Supplier addSupplier(Supplier supplier);

    abstract List<Supplier> getAllSupplier();

    abstract void delete(Long id);

    abstract Optional<Supplier> findById(Long id);

}

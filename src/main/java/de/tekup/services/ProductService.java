package de.tekup.services;

import java.util.List;
import java.util.Optional;

import de.tekup.modeles.Product;

public interface ProductService {

    abstract Product addProduct(Product p);

    abstract List<Product> getAllProduct();

    abstract void delete(Long id);

    abstract Optional<Product> findById(Long id);

    Product saveProduct(Product product);

    List<Product> findByCategoryId(Long categoryId);

    List<Product> findBySubCategoryId(Long subCategoryId);

}
